package com.peqconsultores.demo.senderapp

import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        val receiverPackage = "com.peqconsultores.demo.receiverapp"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnSend.setOnClickListener {

            val origin = etOrigin.text?.toString()?.trim()
            val driverCode = etDriverCode.text?.toString()?.trim()
            val destinationPhone = etDestinationPhone.text?.toString()?.trim()
            var destinationSecondPhone = etSecondDestinationPhone.text?.toString()?.trim()
            val appOrigin = etAppOrigin.text?.toString()?.trim()
            val versionAppOrigin = etVersionAppOrigin.text?.toString()?.trim()
            val order = etOrder.text?.toString()?.trim()
            val client = etClient.text?.toString()?.trim()
            val terminal = etTerminal.text?.toString()?.trim()
            val oficina = etOficina.text?.toString()?.trim()
            val centro = etCentro.text?.toString()?.trim()
           // val flag = etTerminal.text?.toString()?.trim()


            validateData(origin, driverCode, destinationPhone,destinationSecondPhone,appOrigin, versionAppOrigin, order, client, terminal,oficina,centro)
        }
    }

    private fun validateData(
        origin: String? = null,
        driverCode: String? = null,
        destinationPhone: String? = null,
        destinationSecondPhone: String?,
        appOrigin: String? = null,
        versionAppOrigin: String? = null,
        order: String? = null,
        client: String? = null,
        terminal: String? = null,
        oficina: String?,
        centro: String?
        ) {
        val errors = mutableListOf<InputError>()
        if (origin.isNullOrEmpty()) errors.add(InputError("Ingrese un origen válido", R.id.tilOrigin))
        if (driverCode.isNullOrEmpty()) errors.add(InputError("Ingrese un código de conductor válido", R.id.tilDriverCode))
        if (destinationPhone.isNullOrEmpty()) errors.add(InputError("Ingrese un teléfono de destino válido", R.id.tilDestinationPhone))
        if (appOrigin.isNullOrEmpty()) errors.add(InputError("Ingrese la aplicación de origen válida", R.id.tilAppOrigin))
        if (versionAppOrigin.isNullOrEmpty()) errors.add(InputError("Ingrese la versión de la aplicación de origen válido", R.id.tilVersioAppOrigin))
        if (order.isNullOrEmpty()) errors.add(InputError("Ingrese un número de pedido válido", R.id.tilOrder))
        if (client.isNullOrEmpty()) errors.add(InputError("Ingrese un cliente válido", R.id.tilClient))
        if (terminal.isNullOrEmpty()) errors.add(InputError("Ingrese un terminal válido", R.id.tilTerminal))

        if (errors.isEmpty()) {
            sendData(origin!!, driverCode!!, destinationPhone!!, destinationSecondPhone,appOrigin!!, versionAppOrigin!!, order!!, client!!, terminal!!,oficina,centro)
        } else {
            showErrors(errors)
        }
    }

    private fun showErrors(errors: List<InputError>) {
        errors.forEach { findViewById<TextInputLayout>(it.id).error = it.error }
    }

    private fun sendData(
        origin: String,
        driverCode: String,
        destinationPhone: String,
        destinationSecondPhone: String?,
        appOrigin: String,
        versionAppOrigin: String,
        order: String,
        client: String,
        terminal: String,
        oficina: String?,
        centro: String?
    ) {
        try {
            var flag = "false"
            if(!destinationSecondPhone.isNullOrEmpty() && destinationSecondPhone != "") flag = "true"
           /* startActivity(Intent(ACTION_VIEW).apply {
                data = Uri.parse("receiverapp://data/?origin=$origin&driver_code=$driverCode&destination_phone=$destinationPhone&app_origin=$appOrigin&version_app_origin=$versionAppOrigin&order=$order&client=$client&terminal=$terminal")
                `package` = receiverPackage
            })*/
            startActivity(Intent(ACTION_VIEW).apply {
                data = Uri.parse("receiverapp://data/?origin=$origin&driver_code=$driverCode&destination_phone=$destinationPhone&destination_second_phone=$destinationSecondPhone&app_origin=$appOrigin&version_app_origin=$versionAppOrigin&order=$order&client=$client&terminal=$terminal&oficina_sll=$oficina&centro=$centro&flag=$flag")
                `package` = receiverPackage
            })
        } catch (ex: Exception) {
            startActivity(Intent(ACTION_VIEW).apply {
                data = Uri.parse("https://play.google.com/store/apps/details?id=$receiverPackage")
            })
        }
    }

}