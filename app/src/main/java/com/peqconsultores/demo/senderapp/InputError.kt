package com.peqconsultores.demo.senderapp

/**
 * Created by jesusabv93 on 06/04/2019.
 */
data class InputError(
    val error: String,
    val id: Int
)